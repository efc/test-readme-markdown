<?php
/*
Plugin Name:            Test Readme Markdown
Description:            Does nothing at all.
Plugin URI:             https://bitbucket.org/efc/test-readme-markdown
Bitbucket Plugin URI:   https://bitbucket.org/efc/test-readme-markdown
Bitbucket Branch:       master
Version:                1.1
Author:                 Eric Celeste
Author URI:             http://eric.clst.org/
License:                The MIT License 
License URI:            http://opensource.org/licenses/MIT
*/

# do nothing at all